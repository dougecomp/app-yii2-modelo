<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="">

<?php

use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

$dataProvider = new ArrayDataProvider([
    'allModels' => $data,
    /* 'pagination' => [
        'pageSize' => 20
    ], */
    'sort' => [
        'attributes' => [
            'id', 'nome'
        ]
    ]

]);

$gridColumns = [
    // ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'id',
        //'pageSummary' => 'Page Total',
        'vAlign'=>'middle',
        'headerOptions'=>['class'=>'kv-sticky-column'],
        'contentOptions'=>['class'=>'kv-sticky-column'],
        'editableOptions'=>['header'=>'Name', 'size'=>'md', 'name'=>'id']
    ],
    [
        'attribute' => 'nome',
        'value' => function($model, $key, $index, $widget) {
            return $model['nome'];
        },
    ],
    /* [
        'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'nome', 
        'vAlign'=>'middle',
    ], */
    /* [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => true,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { return '#'; },
        'viewOptions'=>['title'=>'Visualizar', 'data-toggle'=>'tooltip'],
        'updateOptions'=>['title'=>'Atualizar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['title'=>'Excluir', 'data-toggle'=>'tooltip'], 
    ], */
    // ['class' => 'kartik\grid\CheckboxColumn']
    ];
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    /* 'beforeHeader'=>[
        [
            'columns'=>[
                ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
                ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
                ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ], */
    'toolbar' =>  [
        /* ['content'=>
            Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('app', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
            Html::a('&lt;i class="glyphicon glyphicon-repeat">&lt;/i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Reset Grid')])
        ],
        '{export}',
        '{toggleData}' */
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['scrollingTop' => true],
    'showPageSummary' => false,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY
    ],
]);

?>
    
</div>
